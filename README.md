# NumberEight iOS Examples

In here you will find several sample apps to showcase how to use the [NumberEight SDK](https://docs.numbereight.me).

* `MainExample` is a small UI-based app which shows current Activity.
* `InsightsExample` is a simple UI app which records context with NumberEight Insights.
* `DevicePositionDetection` prints the current device position to Logcat.
* `SituationAndPlace` prints the current situation and place together to Logcat.
* `ContextMusic` prints music playlists to Logcat depending on the user's context, although the only playlists you'll be getting is Rick Astley...
* `WorkplaceHealth` displays a notification if you've been sitting at work for more than half an hour without a rest break.

All examples except `MainExample` and `InsightsExample` send their output to the console - nothing will appear on your device's screen!

### Adding your developer API key

You will need to [**generate a developer key**](https://portal.eu.numbereight.me/keys) to authenticate with.

The recommended way to run these examples with a real API key is to create a new file in each project called `NumberEight-Info.plist`.

Inside the file, paste the following:

    #<?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    <plist version="1.0">
    <dict>
        <key>API_KEY</key>
        <string>insert_your_developer_key_here</string>
    </dict>
    </plist>

Then specify `nil` as your API key: `[NEXNumberEight startWithApiKey:nil]`

Alternatively, replace all mentions of `REPLACE_WITH_DEVELOPER_KEY` with your key. This is generally not a good practice, as you could accidentally end up adding your API key to a public git repository!

### Documentation

See the [official documentation](http://docs.numbereight.ai/) for guides on setting up your own project and a list of what can be detected.

### Maintainers

* [Matthew Paletta](matt@numbereight.me)
* [Chris Watts](chris@numbereight.me)

