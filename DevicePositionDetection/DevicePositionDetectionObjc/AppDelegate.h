//
//  AppDelegate.h
//  DevicePositionDetectionObjc
//
//  Created by Oliver Kocsis on 14/12/2018.
//  Copyright © 2018 NumberEight. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

