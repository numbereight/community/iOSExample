//
//  ViewController.swift
//  MainExample
//
//  Created by Chris Watts on 12/12/2018.
//  Copyright © 2018 NumberEight. All rights reserved.
//

import UIKit
import Insights

class ViewController: UIViewController {
    @IBOutlet weak var markerButton: UIButton!
    @IBOutlet weak var markerAdded: UILabel!
    
    @IBAction func didPressMarkerButton(_ sender: Any) {
        Insights.addMarker("button_press")
        
        UIView.animate(withDuration: 0.1, animations: {
            self.markerAdded.alpha = 1
        }) { (success) in
            if !success {
                self.markerAdded.alpha = 0
                return
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                UIView.animate(withDuration: 0.5, animations: {
                    self.markerAdded.alpha = 0
                })
            }
        }
    }
}

