//
//  AppDelegate.swift
//  InsightsExample
//
//  Created by Chris Watts on 30/10/2019.
//  Copyright © 2019 NumberEight. All rights reserved.
//

import UIKit
import NumberEight
import Insights

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        let token = NumberEight.start(withApiKey: nil,
                                      launchOptions: launchOptions,
                                      consentOptions: ConsentOptions.withConsentToAll(),
                                      facingAuthorizationChallenges: { (authSource, resolver) in
                                          switch authSource {
                                          case .location:
                                              // Show default permissions dialog to user
                                              resolver.requestAuthorization()
                                          default:
                                              break
                                          }
                                      }) { (success, error) in
            if let error = error {
                print("NumberEight failed to start with an error: \(error)")
            } else if success {
                print("NumberEight started successfully.")
            } else {
                print("NumberEight failed to start with an unknown error.")
            }
        }

        Insights.startRecording(apiToken: token) { (result) in
            if case .failure(let error) = result {
                print("NumberEight Insights failed to start with an error: \(error)")
            } else {
                print("NumberEight Insights started successfully.");
            }
        }
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

}
