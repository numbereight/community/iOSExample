//
//  Snippets.m
//  InsightsExample
//
//  Created by Chris Watts on 25/02/2020.
//  Copyright © 2020 NumberEight. All rights reserved.
//

/// Begin CodeSnippet: ImportInsights
@import Insights;
/// End CodeSnippet: ImportInsights

#import <UIKit/UIKit.h>

@interface MyInsightsAppObjC : UIResponder <UIApplicationDelegate>

@end

@implementation MyInsightsAppObjC
/// Begin CodeSnippet: InitializeInsights
-(BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    NEXAPIToken* token = [NEXNumberEight startWithApiKey:@"REPLACE_WITH_DEVELOPER_KEY"
                                           launchOptions:launchOptions
                                          consentOptions:[NEXConsentOptions withConsentToAll]
                                              completion:^(BOOL success, NSError* _Nullable error) {
        if (error != nil) {
            NSLog(@"NumberEight failed to start with an error: %@ %@", error, [error userInfo]);
        } else if (success) {
            NSLog(@"NumberEight started successfully.");
        } else {
            NSLog(@"NumberEight failed to start with an unknown error.");
        }
    }];
    
    // Start recording insights
    [NEXInsights startRecordingWithAPIToken:token
                                    onStart:^(BOOL success, NSError* _Nullable error) {
       if (error != nil) {
            NSLog(@"NumberEight Insights failed to start with an error: %@ %@", error, [error userInfo]);
        } else {
            NSLog(@"NumberEight Insights started successfully.");
        }
    }];

    return YES;
}
/// End CodeSnippet: InitializeInsights
@end

/// Begin CodeSnippet: InsightsMarkerEvents
@interface MyInsightsViewController : UIViewController
@end

@implementation MyInsightsViewController

-(void)viewDidLoad:(BOOL)animated {
    [super viewDidLoad];
    NSError* error = nil;
    [NEXInsights addMarkerWithName:@"screen_viewed" error:&error];
}

-(void)purchaseMade:(int)value {
    NSError* error = nil;
    [NEXInsights addMarkerWithName:@"in_app_purchase"
                        parameters:@{
                            @"value": @(value)
                        }
                             error:&error];
}

-(void)songPlayedEntitled:(NSString*)title withArtist:(NSString*)artist withGenre:(NSString*)genre {
    NSError* error = nil;
    [NEXInsights addMarkerWithName:@"song_played"
                        parameters:@{
                            @"title": title,
                            @"artist": artist,
                            @"genre": genre
                        }
                             error:&error];
}

@end
/// End CodeSnippet: InsightsMarkerEvents

@interface InsightsSnippets : UIViewController
@end

@implementation InsightsSnippets
-(void)useDeviceId:(NEXAPIToken*)token {
    /// Begin CodeSnippet: InsightsSpecifyDeviceID
    NEXRecordingConfig* config = [NEXRecordingConfig defaultConfig];
    NSString* deviceId = @"insert_custom_device_or_user_id_here_if_required";
    config.deviceId = deviceId;

    [NEXInsights startRecordingWithAPIToken:token config:config onStart:^(BOOL success, NSError* _Nullable error) {
       if (error != nil) {
            NSLog(@"NumberEight Insights failed to start with an error: %@ %@", error, [error userInfo]);
        } else {
            NSLog(@"NumberEight Insights started successfully.");
        }
    }];
    /// End CodeSnippet: InsightsSpecifyDeviceID
}

-(void)addContexts:(NEXAPIToken*)token {
    /// Begin CodeSnippet: InsightsSpecifyConfig
    NEXRecordingConfig* config = [NEXRecordingConfig defaultConfig];

    // Add the Device Movement context to the list of recorded topics
    config.topics = [NSSet setWithArray:@[kNETopicDeviceMovement]];

    // Put a smoothing filter on the Device Movement context
    NSMutableDictionary *filters = config.filters.mutableCopy;
    filters[kNETopicDeviceMovement] = [NEXParameters.sensitivitySmoother and:NEXParameters.changesOnly].filter;
    config.filters = filters;

    [NEXInsights startRecordingWithAPIToken:token config:config onStart:^(BOOL success, NSError* _Nullable error) {
       if (error != nil) {
            NSLog(@"NumberEight Insights failed to start with an error: %@ %@", error, [error userInfo]);
        } else {
            NSLog(@"NumberEight Insights started successfully.");
        }
    }];
    /// End CodeSnippet: InsightsSpecifyConfig
}

@end
