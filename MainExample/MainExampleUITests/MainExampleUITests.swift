//
//  MainExampleUITests.swift
//  MainExampleUITests
//
//  Created by Chris Watts on 14/06/2021.
//  Copyright © 2021 NumberEight. All rights reserved.
//

import XCTest

class MainExampleUITests: XCTestCase {

    override func setUpWithError() throws {
        continueAfterFailure = false
        XCUIApplication().launch()
    }

    override func tearDownWithError() throws {}

    func testTimeToFirstContext() throws {
        let indicator = XCUIApplication().activityIndicators.firstMatch
        
        let isNotHittable = NSPredicate(format: "hittable != 1")
        let expectation = expectation(for: isNotHittable, evaluatedWith: indicator, handler: nil)
        wait(for: [expectation], timeout: 10.0)
    }

}
