import NumberEight

class MyClass {
/// Begin CodeSnippet: NumberEightGettingStarted
func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    NumberEight.start(withApiKey: "REPLACE_WITH_DEVELOPER_KEY",
                      launchOptions: launchOptions,
                      consentOptions: ConsentOptions.withConsentToAll()) { (success, error) in
        if let error = error {
            print("NumberEight failed to start with an error: \(error)")
        } else if success {
            print("NumberEight started successfully.")
        } else {
            print("NumberEight failed to start with an unknown error.")
        }
    }
    return true
}
/// End CodeSnippet: NumberEightGettingStarted
}
