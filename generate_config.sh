#!/bin/bash
set -euo pipefail

if command -v $(which xcodebuild) &> /dev/null; then
	echo "Found Xcode"
else
	echo "Failed to find Xcode.  Install from the App Store"
	exit 1
fi

all_schemes=`xcodebuild -workspace iOSExample.xcworkspace -list | grep -E "^        *" | grep "Example"`
all_configs="Release Debug"
all_sdk="iphonesimulator iphoneos"

IOS_CONFIG_FILE=".ios-gitlab-ci.yml"
rm -f ${IOS_CONFIG_FILE}

echo \
"include:
  - project: 'numbereight/operations/ci-scripts'
    file: '/ios/ci.yml'

" >> $IOS_CONFIG_FILE

for config in $all_configs; do
	for sdk in $all_sdk; do
		for target in $all_schemes; do
            if [[ $target == Pods* ]];
            then
                continue;
            fi
			echo "Generating Target:${target}-${config}-${sdk}"
echo \
"build:${target}-${config}-${sdk}:
  extends: .ne-macos-runner
  stage: build
  before_script:
    - LANG=en_US.UTF-8 pod install --repo-update
  script:
    - xcodebuild -allowProvisioningUpdates ONLY_ACTIVE_ARCH=YES CONFIGURATION=${config} -workspace iOSExample.xcworkspace -scheme \"${target}\" -sdk \"${sdk}\" analyze | xcpretty
  interruptible: true
" >> $IOS_CONFIG_FILE
		done
	done
done
