//
//  AppDelegate.swift
//  AudiencesExample
//
//  Created by Chris Watts on 12/08/2020.
//  Copyright © 2020 NumberEight Technologies Ltd. All rights reserved.
//

import UIKit
import NumberEight
import Audiences

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let token = NumberEight.start(withApiKey: nil,
                                      launchOptions: launchOptions,
                                      consentOptions: ConsentOptions.withConsentToAll(),
                                      facingAuthorizationChallenges: { (authSource, resolver) in
                                          switch authSource {
                                          case .location:
                                              // Show default permissions dialog to user
                                              resolver.requestAuthorization()
                                          default:
                                              break
                                          }
                                      }) { (success, error) in
            if let error = error {
                print("NumberEight failed to start with an error: \(error)")
            } else if success {
                print("NumberEight started successfully.")
            } else {
                print("NumberEight failed to start with an unknown error.")
            }
        }

        Audiences.startRecording(apiToken: token) { (result) in
            if case .failure(let error) = result {
                print("NumberEight Audiences failed to start with an error: \(error)")
            } else {
                print("NumberEight Audiences started successfully.");
            }
        }
        return true
    }

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}
