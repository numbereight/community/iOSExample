//
//  Snippets.swift
//  AudiencesExample
//
//  Created by Chris Watts on 24/09/2020.
//  Copyright © 2020 NumberEight. All rights reserved.
//

/// Begin CodeSnippet: ImportAudiences
import Audiences
/// End CodeSnippet: ImportAudiences


class MyInsightsAppSwift : UIViewController {
/// Begin CodeSnippet: InitializeAudiences
func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    // Start the SDK
    let token = NumberEight.start(withApiKey: "REPLACE_WITH_DEVELOPER_KEY",
                                  launchOptions: launchOptions,
                                  consentOptions: ConsentOptions.withConsentToAll()) { (success, error) in
        if let error = error {
            print("NumberEight failed to start with an error: \(error)")
        } else if success {
            print("NumberEight started successfully.")
        } else {
            print("NumberEight failed to start with an unknown error.")
        }
    }

    // Start recording audiences
    Audiences.startRecording(apiToken: token) { (result) in
        if case .failure(let error) = result {
            print("NumberEight Audiences failed to start with an error: \(error)")
        } else {
            print("NumberEight Audiences started successfully.");
        }
    }

    return true
}
/// End CodeSnippet: InitializeAudiences

/// Begin CodeSnippet: GetAudiences
func getAudiences() {
    let audiences = Audiences.currentMemberships
    for membership in audiences {
        print("Audiences \(membership.name)")
    }
}

func getAudienceIds() {
    let idList = Audiences.currentIds
    print("Audiences \(idList)") // e.g. ["NE-1-1", "NE-6-2"]
}

func getLiveAudiences() {
    let audiences = Audiences.currentMemberships.filter { $0.liveness == .live }
    for membership in audiences {
        print("Live audiences \(membership.name)")
    }
}
/// End CodeSnippet: GetAudiences
}
