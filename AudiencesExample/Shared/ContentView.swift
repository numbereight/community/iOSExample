//
//  ContentView.swift
//  AudiencesExample
//
//  Created by Chris Watts on 12/08/2020.
//  Copyright © 2020 NumberEight Technologies Ltd. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State var audiencesStr = ""

    let timer = Timer.publish(every: 1, on: .current, in: .common).autoconnect()

    var body: some View {
        Text("\(audiencesStr)")
            .onReceive(timer) { _ in
                let audiences: [String] = []
                if audiences.count == 0 {
                    self.audiencesStr = "No audiences yet…"
                } else {
                    self.audiencesStr = audiences.joined(separator: "\n")
                }
            }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
