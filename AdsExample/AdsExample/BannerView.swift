//
//  BannerView.swift
//  AdsExample
//
//  Created by Chris Watts on 16/02/2023.
//

import Foundation
import GoogleMobileAds
import SwiftUI

struct BannerView: UIViewControllerRepresentable {
    @State private var viewWidth: CGFloat = GADAdSizeBanner.size.width
    private let gamAdUnitID = "/6499/example/banner"

    let gamBanner = GAMBannerView(adSize: GADAdSizeBanner)
    let backgroundColor: UIColor
    
    init(backgroundColor: UIColor = UIColor.clear) {
        self.backgroundColor = backgroundColor
    }

    func makeUIViewController(context: Context) -> some UIViewController {
        let bannerViewController = BannerViewController()
        gamBanner.adUnitID = gamAdUnitID
        gamBanner.rootViewController = bannerViewController
        bannerViewController.view.backgroundColor = backgroundColor
        bannerViewController.view.addSubview(gamBanner)

        return bannerViewController
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        guard viewWidth != .zero else { return }
        gamBanner.adSize = GADCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(viewWidth)
    }
}

protocol BannerViewControllerWidthDelegate: AnyObject {
  func bannerViewController(_ bannerViewController: BannerViewController, didUpdate width: CGFloat)
}

class BannerViewController: UIViewController {
  weak var delegate: BannerViewControllerWidthDelegate?

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    // Tell the delegate the initial ad width.
    delegate?.bannerViewController(
      self, didUpdate: view.frame.inset(by: view.safeAreaInsets).size.width)
  }

  override func viewWillTransition(
    to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator
  ) {
    coordinator.animate { _ in
      // do nothing
    } completion: { _ in
      // Notify the delegate of ad width changes.
      self.delegate?.bannerViewController(
        self, didUpdate: self.view.frame.inset(by: self.view.safeAreaInsets).size.width)
    }
  }
}
