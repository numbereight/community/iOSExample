//
//  MediationNetwork.h
//  AdsExample
//
//  Created by Chris Watts on 24/02/2023.
//
#ifndef MediationNetwork_h
#define MediationNetwork_h

typedef void (^MediatedNetworkUpdatedHandler)(NSString* _Nonnull);

@interface MediationNetwork : NSObject

@property (nonatomic, copy, nonnull) NSString* mediatedNetwork;
@property (readonly, copy, nonnull) NSArray<NSString*>* mediationOptions;

- (_Nonnull instancetype)init;

- (void)addMediatedNetworkUpdatedHandler:(_Nonnull MediatedNetworkUpdatedHandler)handler;
- (void)removeMediatedNetworkUpdatedHandler:(_Nonnull MediatedNetworkUpdatedHandler)handler;

@end

#endif /* MediationNetwork_h */
