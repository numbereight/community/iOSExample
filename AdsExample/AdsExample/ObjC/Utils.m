//
//  Utils.m
//  AdsExample
//
//  Created by Chris Watts on 27/02/2023.
//

#import <Foundation/Foundation.h>
#import "Utils.h"

@import PromiseKit;

int cancellationCode = 444;
int failureCode = 500;

NSError* _Nonnull cancellationError(void) {
    return cancellationErrorWithDetails(nil);
}

NSError* _Nonnull cancellationErrorWithDetails(NSDictionary* _Nullable details) {
    NSMutableDictionary* userInfo = [NSMutableDictionary dictionaryWithDictionary:details];
    userInfo[NSLocalizedDescriptionKey] = @"Operation cancelled.";
    return [NSError errorWithDomain:@"ai.numbereight.adsexample"
                               code:cancellationCode
                           userInfo:userInfo];
}

BOOL isCancellationError(NSError* _Nonnull error) {
    return error.code == cancellationCode;
}

NSError* _Nonnull adLoadError(NSString* _Nonnull info) {
    NSMutableDictionary* userInfo = [[NSMutableDictionary alloc] init];
    userInfo[NSLocalizedDescriptionKey] = info;
    return [NSError errorWithDomain:@"ai.numbereight.adsexample"
                               code:failureCode
                           userInfo:userInfo];
}

AnyPromise* _Nonnull wrapPromise(AnyPromise* _Nullable __strong * _Nonnull targetPromise,
                                 PMKResolver _Nullable __strong * _Nonnull targetResolver,
                                 void (^ _Nonnull action)(AnyPromise* _Nonnull, __nonnull PMKResolver)) {
    if ([*targetPromise pending]) {
        return *targetPromise;
    }

    PMKResolver resolver;
    AnyPromise* promise = [[AnyPromise alloc] initWithResolver:&resolver];
    *targetPromise = promise;
    *targetResolver = resolver;
    action(promise, resolver);

    return promise;
}
