//
//  Utils.h
//  AdsExample
//
//  Created by Chris Watts on 27/02/2023.
//

#import "AdsExample-Swift.h"

#ifndef Utils_h
#define Utils_h

@import PromiseKit;

NSError* _Nonnull cancellationError(void);
NSError* _Nonnull cancellationErrorWithDetails(NSDictionary* _Nullable details);

BOOL isCancellationError(NSError* _Nonnull error);

NSError* _Nonnull adLoadError(NSString* _Nonnull info);

AnyPromise* _Nonnull wrapPromise(AnyPromise* _Nullable __strong * _Nonnull targetPromise,
                                 PMKResolver _Nullable __strong * _Nonnull targetResolver,
                                 void (^ _Nonnull action)(AnyPromise* _Nonnull, __nonnull PMKResolver));

#endif /* Utils_h */
