//
//  test.m
//  AdsExample
//
//  Created by Chris Watts on 06/03/2023.
//

#import <Foundation/Foundation.h>

#import "AdsExample-Swift.h"

void test(void) {
    GoogleAdManagerObjC* loader = [[GoogleAdManagerObjC alloc] init];
    GoogleAdManager* swiftLoader = [GoogleAdManager init];
    [loader name];
    [swiftLoader name];
}
