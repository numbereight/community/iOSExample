//
//  AdLoader.h
//  AdsExample
//
//  Created by Chris Watts on 06/03/2023.
//
@import Foundation;

#ifndef AdLoader_h
#define AdLoader_h

#if __has_attribute(warn_unused_result)
# define SWIFT_WARN_UNUSED_RESULT __attribute__((warn_unused_result))
#else
# define SWIFT_WARN_UNUSED_RESULT
#endif

@class AnyPromise;

@protocol AdLoader <NSObject>

@property (nonatomic, readonly, copy) NSString * _Nonnull name;

@property (nonatomic, copy) NSString * _Nonnull mediatedNetwork;
@property (nonatomic, readonly, copy) NSArray<NSString *> * _Nonnull mediationOptions;

- (AnyPromise * _Nonnull)loadBanner SWIFT_WARN_UNUSED_RESULT;
- (void)closeBanner;

- (AnyPromise * _Nonnull)loadInterstitial SWIFT_WARN_UNUSED_RESULT;
- (void)cancelInterstitial;

@end

#endif /* AdLoader_h */
