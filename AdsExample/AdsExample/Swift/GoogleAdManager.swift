//
//  GoogleAdManager.swift
//  AdsExample
//
//  Created by Chris Watts on 16/02/2023.
//

import Foundation

import Audiences
import GoogleMobileAds
import PromiseKit
import CancellablePromiseKit
import SwiftUI

final class GoogleAdManager: MediationNetwork, AdLoader {
    @objc static let interstitialAdUnitId = "/22836750855/test-mobile-video-interstitial"

    var name = "Google Ad Manager"
    
    private let bannerView: GAMBannerView
    private let viewController = UIViewController()
    
    private var bannerPromise: CancellablePromise<Void>? = nil
    private var bannerResolver: Resolver<Void>? = nil
    private var interstitialPromise: CancellablePromise<Void>? = nil
    private var interstitialResolver: Resolver<Void>? = nil

    init(bannerView: GAMBannerView) {
        self.bannerView = bannerView
        super.init()
        
        // Initialize the Google Mobile Ads SDK.
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        setupAdUnits();
    }

    private func setupAdUnits() {
        bannerView.delegate = self
    }

    private func generateRequest() -> GAMRequest {
        /// Begin CodeSnippet: CollectMemberships
        // Collect audiences for all liveness types
        let memberships = Audiences.currentMemberships
        let habitual = memberships.filter { $0.liveness == .habitual }
        let live = memberships.filter { $0.liveness == .live }
        let today = memberships.filter { $0.liveness == .happenedToday }
        let thisWeek = memberships.filter { $0.liveness == .happenedThisWeek }
        let thisMonth = memberships.filter { $0.liveness == .happenedThisMonth }
        /// End CodeSnippet: CollectMemberships

        /// Begin CodeSnippet: MakeGAMRequest
        // Create an ad request filled with NumberEight Audiences Taxonomy IDs
        // Audiences build up over time: live audiences are available after a few seconds, whereas
        // habitual ones can take several days.
        // While testing, it is recommended to continually make new ad requests to see as many
        // audiences as possible.
        let request = GAMRequest()
        request.customTargeting = [
            "ne_habitual" : habitual.map { $0.id }.joined(separator: ","),
            "ne_live" : live.map { $0.id }.joined(separator: ","),
            "ne_today" : today.map { $0.id }.joined(separator: ","),
            "ne_this_week" : thisWeek.map { $0.id }.joined(separator: ","),
            "ne_this_month" : thisMonth.map { $0.id }.joined(separator: ",")
        ]
        /// End CodeSnippet: MakeGAMRequest

        return request
    }
}

extension GoogleAdManager {
    func loadBanner() -> AnyPromise {
        return AnyPromise(wrapPromise(&bannerPromise, resolver: &bannerResolver) { _, _  in
            let request = generateRequest()
            /// Begin CodeSnippet: LoadGAMBannerAd
            bannerView.load(request)
            /// End CodeSnippet: LoadGAMBannerAd
        })
    }

    func closeBanner() {
        bannerPromise?.cancel()
        bannerView.isHidden = true
    }

    func loadInterstitial() -> AnyPromise {
        return AnyPromise(wrapPromise(&interstitialPromise, resolver: &interstitialResolver) { promise, resolver in
            let request = generateRequest()
            GAMInterstitialAd.load(withAdManagerAdUnitID: GoogleAdManager.interstitialAdUnitId,
                                   request: request) { ad, error in
                if let error = error {
                    resolver.reject(error)
                    return
                }
                
                guard let viewController = self.bannerView.rootViewController else {
                    resolver.reject(AdLoaderError.adLoadFailed("No view controller available to show interstitial ad"))
                    return
                }
                
                guard let ad = ad else {
                    resolver.reject(AdLoaderError.adLoadFailed("Ad is null"))
                    return
                }
                
                if promise.isPending {
                    ad.present(fromRootViewController: viewController)
                    resolver.fulfill_()
                }
            }
        })
    }

    func cancelInterstitial() {
        interstitialPromise?.cancel()
    }
}

extension GoogleAdManager: GADBannerViewDelegate {
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
        if let promise = bannerPromise, let resolver = bannerResolver, promise.isPending {
            bannerView.isHidden = false
            resolver.fulfill_()
        }
    }

    func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
        if let resolver = bannerResolver {
            resolver.reject(error)
        }
    }
}
