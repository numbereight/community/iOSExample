//
//  AdsMenu.swift
//  AdsExample
//
//  Created by Chris Watts on 15/02/2023.
//

import SwiftUI

class AdsMenuViewModel: ObservableObject {
    let adNetworks: [String: any AdLoader]

    @Published var adNetwork: String? = nil
    @Published var mediatedNetwork: String? = nil
    
    var adLoader: (any AdLoader)? {
        get {
            guard let network = adNetwork else { return nil }
            return adNetworks[network]
        }
    }
    
    init(bannerView: BannerView) {
        let googleAdManager = GoogleAdManager(bannerView: bannerView.gamBanner)
        adNetworks = [
            googleAdManager.name: googleAdManager,
        ]
        adNetwork = Array(adNetworks.keys)[0]
    }
}

struct AdsMenu: View {
    let controlWidth: CGFloat = 170
    let controlHeight: CGFloat = 30
    
    let bannerView: BannerView
    @ObservedObject var viewModel: AdsMenuViewModel
    
    @State var loadingBanner = false
    @State var loadingInterstitial = false
    @State var errorMessage = ""
    
    init() {
        bannerView = BannerView(backgroundColor: UIColor.systemGray5)
        viewModel = AdsMenuViewModel(bannerView: bannerView)
    }
    
    var body: some View {
        ZStack(alignment: .bottom) {
            AccentShape()
                .foregroundColor(Color.init("SecondaryColor"))
                .ignoresSafeArea()
            VStack(spacing: 30) {
                bannerView.frame(width: 320, height: 50, alignment: .center)
                VStack(spacing: 20) {
                    VStack(spacing: 0) {
                        Text("Ad Network")
                        Picker(selection: $viewModel.adNetwork) {
                            ForEach(Array(viewModel.adNetworks.keys.sorted()), id: \.self) { network in
                                Text(network).tag(network)
                            }
                        } label: {}
                            .frame(minWidth: controlWidth, minHeight: controlHeight)
                    }
                    if (viewModel.adLoader?.mediationOptions != nil) {
                        VStack(spacing: 0) {
                            Text("Mediated Network")
                            Picker(selection: $viewModel.mediatedNetwork) {
                                ForEach(viewModel.adLoader?.mediationOptions.sorted() ?? [], id: \.self) { mediation in
                                    Text(mediation).tag(mediation)
                                }
                            } label: {}
                                .frame(minWidth: controlWidth, minHeight: controlHeight)
                                .id(viewModel.adNetwork)
                        }
                    }
                }
                
                VStack {
                    Button {
                        guard let loader = viewModel.adLoader else { return }
                        loadingBanner = true
                        loader.loadBanner()
                            .catch({ error in
                                errorMessage = error.localizedDescription
                            })
                            .finally {
                                loadingBanner = false
                            }
                    } label: {
                        Text("Load Banner").frame(width: controlWidth, height: controlHeight)
                    }.disabled(loadingBanner).buttonStyle(BaseButton()).padding(5)
                    
                    Button {
                        guard let loader = viewModel.adLoader else { return }
                        loadingInterstitial = true
                        loader.loadInterstitial()
                            .catch({ error in
                                errorMessage = error.localizedDescription
                            })
                            .finally {
                                loadingInterstitial = false
                            }
                    } label: {
                        Text("Load Interstitial").frame(width: controlWidth, height: controlHeight)
                    }.disabled(loadingInterstitial).buttonStyle(BaseButton()).padding(5)
                }
                
                Button {
                    for loader in viewModel.adNetworks.values {
                        loader.closeAll()
                    }
                } label: {
                    Text("Clear Ads").frame(width: controlWidth, height: controlHeight)
                }.buttonStyle(BaseButton())
                
                VStack(spacing: 5) {
                    Text("Extra Info").bold()
                    TextEditor(text: $errorMessage)
                        .disabled(true)
                        .frame(height: 100)
                        .onAppear() {
                            UITextView.appearance().backgroundColor = .clear
                        }
                }.padding(5)
                    .background(Color.init(UIColor.systemGray5))
                    .padding(EdgeInsets(top: 0, leading: 30, bottom: 0, trailing: 30))
            }.padding(.bottom, 30)
        }
    }
}

struct AdsMenu_Previews: PreviewProvider {
    static var previews: some View {
        AdsMenu()
    }
}
